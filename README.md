# project-doors-3

project_doors - created in remote GitLab; then cloned to --> local;
   * see README.md


____________________________________________________
Workflow:

1. create ISSUE
2. create BRANCH right on issue page (tied with the issue itself)
3. create MERGE REQUEST from issue window in gitlab; + "WIP" (Work In Progress) status and label "in progress"
4. checkout branch you created by creating MR -?
5. push CHANGES/commits?
6. Paste MR link with changes for code review to #developers
7. discuss: get comments <-> comment back <-> push commits -> mark resolved -> “Resolve dissolution”
8. ! Before open Merge(pull) request must merge all new target branch commits to your branch: merge develop_branch -> into feature_branch
9. merge feature_branch -> into develop_branch
10. delete source branch
_________________

